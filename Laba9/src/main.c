#include "lib.h"

/**
 * Точка входу
 * @return код завершення (0)
 */
int main(void)
{
	int number = 100;
	int result = sqrt_from_number(number);

	int matrix[SIZE_OF_ROWS_AND_COLUMNS]
		  [SIZE_OF_ROWS_AND_COLUMNS] = { { 2, 3, 4 },
						 { 5, 4, 1 },
						 { 0, 2, 3 } },
		 matrix_string[SIZE_OF_ROWS_AND_COLUMNS *
			       SIZE_OF_ROWS_AND_COLUMNS],
		 result_matrix[SIZE_OF_ROWS_AND_COLUMNS]
			      [SIZE_OF_ROWS_AND_COLUMNS] = { { 0, 0, 0 },
							     { 0, 0, 0 },
							     { 0, 0, 0 } };

	for (int i = 0; i < SIZE_OF_ROWS_AND_COLUMNS; i++) {
		for (int j = 0; j < SIZE_OF_ROWS_AND_COLUMNS; j++) {
			matrix_string[i * SIZE_OF_ROWS_AND_COLUMNS + j] =
				matrix[i][j];
		}
	}

	multiple_matrix(matrix_string, result_matrix);

	return 0;
}
