# Лабораторна робота №9 - Модульне тестування


## Мета

-  Переробити проект на багатофайлову структуру, в якої:
   - main.c - склаадється з тестового запуску функцій для завідомо відомих даних
   - lib.c - складається з реалізації розроблених функій
   - lib.h - складається з прототипів розроблених функцій, що документовані
-  Для попередньо розроблених функцій (Laba7) додати методи – модульні тести, що
   демонструють коректність роботи розробленого функціоналу. Розроблені методи мають
   перевірити коректність функціонування функцій на наборі заздалегідь визначених вхідних-
   вихідних даних. 

## 1 Вимоги

### 1.1 Розробник

Інформація про розробника:  

- Никифоренко Валерія Ігорівна;
- КН-921б;

### 1.2 Загальне завдання

- Переробити проект на багатофайлову структуру
- Зробити модульні тести до программи, за допомогою сторонної бібліотеки <check.h>

### 1.3 Задача

- main.c переробити для модульних тестів, та зробити декілька тестів з функцій лабораторної роботи номер 7
- Переробити Makefile на багатофайлову структуру й додати туди методи, що будуть генерувати нам модульні тести
- Зробити doxygen  документацію до файла lib.h

> Для того, щоб запустити программу та побачити її роботу, достатьно виповнити bash команду:
>
> `./dist/main.bin`. Программа повинна сказати вам, що тести пройшли без помилок та видати звіт о виконанні программи у терміналі та в сторонньому файлі `coverage.html`.  Якщо ви бажаете змінити результат виконання программи - запустіть її через дебаггер й потсавте брейкпоїнти в потрібних вам місцях.
>
> Зробити документацію до проекта можна за допомогую команди `make docgen`.

Приклад Makefile для данної лаборатоорної роботи:

```c (lldb) b 111
BINARY_SRC = dist
SOURCES_SRC = src
SOURCES = *.c

all: format clean prep compile
	LLVM_PROFILE_FILE="dist/main.profraw" ./$(BINARY_SRC)/main.bin
	llvm-profdata merge -sparse $(BINARY_SRC)/main.profraw -o $(BINARY_SRC)/main.profdata
	llvm-cov report $(BINARY_SRC)/main.bin -instr-profile=$(BINARY_SRC)/main.profdata $(SOURCES_SRC)/$(SOURCES)
	llvm-cov show $(BINARY_SRC)/main.bin -instr-profile=$(BINARY_SRC)/main.profdata $(SOURCES_SRC)/$(SOURCES) --format html > $(BINARY_SRC)/coverage.html
clean:
	rm -rf $(BINARY_SRC)
prep:
	mkdir $(BINARY_SRC)
compile: main.bin

main.bin: $(SOURCES_SRC)/$(SOURCES)
	$(CC) $(COPTS) $(SOURCES_SRC)/$(SOURCES) -fprofile-instr-generate -fcoverage-mapping -o ./$(BINARY_SRC)/$@ -lcheck -lm -lrt -lpthread -lsubunit
format: $(SOURCES_SRC)/.clang-format
		clang-format $(SOURCES_SRC)/$(SOURCES) -i
docgen:
	doxygen Doxyfile
```

Приклад принципу роботи программи (приклад розробки модульного тесту):

```c (lldb) r
#include "lib.h"
#include <check.h>

START_TEST(sqrt_test)
{
	int number = 144;
	int actual = sqrt_from_number(number);
	int expected = 12;

	ck_assert_int_eq(actual, expected);
}
END_TEST

START_TEST(multiple_matrix_test)
{
	int matrix[SIZE_OF_ROWS_AND_COLUMNS]
		  [SIZE_OF_ROWS_AND_COLUMNS] = { { 2, 3, 4 },
						 { 5, 4, 1 },
						 { 0, 2, 3 } },
		 matrix_string[SIZE_OF_ROWS_AND_COLUMNS *
			       SIZE_OF_ROWS_AND_COLUMNS],
		 result_matrix[SIZE_OF_ROWS_AND_COLUMNS]
			      [SIZE_OF_ROWS_AND_COLUMNS] = { { 0, 0, 0 },
							     { 0, 0, 0 },
							     { 0, 0, 0 } },
		 expected_matrix[SIZE_OF_ROWS_AND_COLUMNS]
				[SIZE_OF_ROWS_AND_COLUMNS] = { { 19, 26, 23 },
							       { 30, 33, 27 },
							       { 10, 14, 11 } };

	for (int i = 0; i < SIZE_OF_ROWS_AND_COLUMNS; i++) {
		for (int j = 0; j < SIZE_OF_ROWS_AND_COLUMNS; j++) {
			matrix_string[i * SIZE_OF_ROWS_AND_COLUMNS + j] =
				matrix[i][j];
		}
	}

	multiple_matrix(matrix_string, result_matrix);

	for (int i = 0; i < SIZE_OF_ROWS_AND_COLUMNS; i++) {
		for (int j = 0; j < SIZE_OF_ROWS_AND_COLUMNS; j++) {
			ck_assert_int_eq(result_matrix[i][j],
					 expected_matrix[i][j]);
		}
	}
}
END_TEST

int main(void)
{
	Suite *s = suite_create("Programming");
	TCase *tc_core = tcase_create("Laba9");

	tcase_add_test(tc_core, sqrt_test);
	tcase_add_test(tc_core, multiple_matrix_test);
	suite_add_tcase(s, tc_core);

	SRunner *sr = srunner_create(s);
	srunner_run_all(sr, CK_VERBOSE);
	int number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
```

Корректний результат виконання программи:

```c
bin/test.bin
Running suite(s): Programming labs
100%: Checks: 2, Failures: 0, Errors: 0
test/test_all.c:10:P:Laba9:sqrt_test:0: Passed
test/test_all.c:43:P:Laba9:multiple_matrix_test:0: Passed
llvm-profdata merge -sparse default.profraw -o default.profdata
llvm-cov report bin/test.bin -instr-profile=default.profdata src/*.c
Filename                      Regions    Missed Regions     Cover   Functions  Missed Functions  Executed       Lines      Missed Lines     Cover
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lab09/src/lib.c                    26                 1    96.15%           4                 0   100.00%          48                 2    95.83%
Laba9/test/test_all.c               1                 0   100.00%           1                 0   100.00%          15                 0   100.00%

Files which contain no functions:
Lab09/src/lib.h                     0                 0         -           0                 0         -           0                 0         -
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
TOTAL                              27                 1    96.30%           5                 0   100.00%          63                 2    96.83%
llvm-cov show bin/test.bin -instr-profile=default.profdata src/*.c -format html > coverage.html

```





## Висновок

На цій лабораторній работі я навчився користуватися модульними тестами, які допоможуть у подальшій розробці програм й не витрачати час на перевірку результатів виконання программи.
