# Лабораторна робота №13. Взаємодія з файлами

## Мета

Отримати навички розробки програм мовою С, які моють у собі взаємодію з файлами

## 1 Вимоги

### 1.1 Розробник

Інформація

- Никифоренко Валерія Ігорівна;
- КН-921Б;

### 1.2 Загальне завдання

- Розробити програму, що в заданому файлі створює горизонтальну синусоїду із заданих
  символів (наприклад, зірочок).
- Амплітуда синусоїди, символ заповнювача та період
  задається у вхідному файлу.

### 1.3 Задача

- Зробити функцію, що буде перераховувати кожен елемент массиву строк, і якщо він збігаеться з точкою графіка синуса, то помітити цю точку на массиві.

- Програма повинна прийняти у якості аргументу шлях до вхідного й вихідного файлу;

  за допомогою команди `./dist/main.bin "./assets/input.txt" "./dist/output.txt"`;

- Максимальна кількість символів, що можна зчитати з файлу = 100 символів.

> Для того, щоб запустити программу та побачити її роботу, достатьно виповнити bash команду:
>
> `./dist/main.bin "./assets/input.txt" "./dist/output.txt"`.
>
> Зробити документацію до проекта можна за допомогую команди `make docgen`.
>
> Зробити модульні тести до ціеї програми можна за допомогою команди `make test`.

Корректний результат виконання программи:

```c
clang-format src/* -i
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping src/lib.c -o lib.o
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping src/main.c -o main.o
mkdir -p dist
clang -fprofile-instr-generate -fcoverage-mapping lib.o main.o -lm -o dist/main.bin
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping -Isrc test/test.c -o test.o
clang -fprofile-instr-generate -fcoverage-mapping lib.o test.o -lcheck -lm -lrt -lpthread -lsubunit -o dist/test.bin
./dist/main.bin "./assets/input.txt" "./dist/output.txt"
Отримую з ./assets/input.txt й кладу в ./dist/output.txt
                                 
                                                                                                   
                                                            *******                                
                                                           *       *                               
                                                          *         *                              
                                                         *           *                             
                                                        *             *                            
                                                       *                                           
                                                                       *                           
                                                      *                 *                          
                                                                                                   
                                                     *                   *                         
                                                    *                                              
                                                                          *                        
                                                   *                       *                       
                                                                                                   
                                                  *                         *                      
                                                                                                   
                                                 *                           *                     
                                                                                                   
                                                 *                            *                    
                                                *                              *                   
                                                                                                   
                                               *                                *                  
                                                                                                   
                   *                          *                                                    
                                                                                                   
                    *                        *                                                     
                                                                                                   
                     *                      *                                                      
                      *                    *                                                       
                                                                                                   
                       *                  *                                                        
                                                                                                   
                        *                *                                                         
                         *              *                                                          
                          *            *                                                           
                                                                                                   
                           *         **                                                            
                            **      *                                                              
                              ******                                                 
```

## Висновок

На цій лабораторній работі я робити з файлами в мові C, та программами що мають аргументи запуску.