#include "lib.h"
#include <check.h>

START_TEST(str_to_digit_test)
{
    /* Ініціалізація змінних */
    double expected = 256;
    char *bufer = malloc(MAX_CHARACTERS);
    double actual = 0;

    /* Отримуємо ввід данних від користувача */
    fgets(bufer, MAX_CHARACTERS, stdin);

    /* Конвертуемо строку в число */
    actual = str_to_digit(bufer);

    /* Перевіряемо дані на валідність */
    ck_assert_int_eq(actual, expected);

    /* Звільняемо памьять */
    free(bufer);
}
END_TEST

int main(void)
{
    Suite *s = suite_create("Programming labs");
    TCase *tc_core = tcase_create("Laba12");

    tcase_add_test(tc_core, str_to_digit_test);
    suite_add_tcase(s, tc_core);

    SRunner *sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
