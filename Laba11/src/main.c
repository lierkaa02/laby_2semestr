#include "lib.h"

/**
 * Точка входу
 * @return код завершення (0)
 */
int main(void)
{
	printf("Лабораторна робота номер 11. Взаємодія з користувачем шляхом механізму\"введення/виведення\". Автор - Никифоренко Валерія Ігорівна\n");

	/* Створення матриці */
	double **matrix =
		(double **)malloc(NUMBER_OF_COLS_AND_ROWS * sizeof(double *));
	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
		*(matrix + i) = (double *)malloc(NUMBER_OF_COLS_AND_ROWS *
						 sizeof(double));
	}

	/* Створення оберненої матриці */
	double **reversed_matrix =
		(double **)malloc(NUMBER_OF_COLS_AND_ROWS * sizeof(double *));
	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
		*(reversed_matrix + i) = (double *)malloc(
			NUMBER_OF_COLS_AND_ROWS * sizeof(double));
	}

	/* Заповнюемо матрицю */
	printf("Заповнюємо матрицю.\n");
	seed_matrix(matrix);

	/* Вивід матриці на єкран */
	printf("Матриця буде такою:\n");
	print_matrix(matrix);

	/* Розраховуємо обернену матрицю */
	bool isset_reversing_matrix = reverse_matrix(matrix, reversed_matrix);

	/* Вивід оберненої матриці на єкран */
	if (isset_reversing_matrix) {
		printf("Обернена матриця буде такою:\n");
		print_matrix(reversed_matrix);
	}

	/* Очищення памьяті після використання  */
	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
		free(*(matrix + i));
		free(*(reversed_matrix + i));
	}
	free(matrix);
	free(reversed_matrix);

	return 0;
}
